const btn = document.getElementById("btn");

btn.addEventListener("click", function onClick(event) {
  const randomColor = Math.floor(Math.random() * 16777215).toString(16);
  document.body.style.background = `#${randomColor}`;
  document.getElementById(
    "clrname"
  ).innerHTML = `Current color: ${randomColor}`;
});
